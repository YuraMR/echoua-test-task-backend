const getJsonFile = path => id => (`${path}${id}.json`);

const compose = (...functions) => value => functions.reduceRight(
  (result, item) => item(result), value
);

export { getJsonFile, compose };
