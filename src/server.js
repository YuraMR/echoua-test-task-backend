import express from 'express';
import cors from 'cors';
import http from 'http';
import config from './config';
import log from './logging';
import api from './api';

const app = express();

app.use(cors());
app.use('/api', api);

const init = async () => {
  const { host, port } = config.app;

  await http.createServer(app).listen(port, host);
  log.info(`The server is up and running at ${host}:${port}`);
};

process.on('unhandledRejection', err => {
  log.error(`An unhandled rejection has occured: ${err}`);
  process.exit(1);
});

init();
