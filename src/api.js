import express from 'express';
import fs from 'fs';
import log from './logging';
import { compose, getJsonFile } from './resources/utils/functions';

const router = express.Router();

// middleware
router.use((req, res, next) => {
  const info = {
    time   : new Date(),
    params : req.params,
    url    : req.url,
    method : req.method,
    headers: req.headers,
  };
  // log all the requests to the API
  log.info(`${JSON.stringify(info)}`);
  next();
});

const getReportById = getJsonFile('src/resources/data/report_');

const reply = compose(fs.createReadStream, getReportById);

router.get('/', (req, res) => res.send('¡Hola Mundo!'));
router.get('/report/:id', (req, res) => reply(req.params.id).pipe(res));

export default router;
