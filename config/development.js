import path from 'path';
// Export
module.exports = {
  app: {
    host: 'localhost',
    port: 8080
  },
  logs: {
    name   : 'echoua-test-task-backend',
    folder : path.join(__dirname, '../logs/'),
    streams: [
      {
        level : 'debug',
        stream: process.stdout // log INFO and above to stdout
      }
    ]
  }
};
